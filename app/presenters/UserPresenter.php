<?php
namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Ublaboo;
use Nette\Application\Routers\Route;


class UserPresenter extends Nette\Application\UI\Presenter
{

    private $database;
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderShow($userId)
    {
        $userId = $this->getUser()->id;
        $user = $this->database->table('authors')->get($userId);
        if (!$user) {
            $this->error('User nebyl nalezen');
        }
        $this->template->user = $user;
    }

    public function actionEdit($userId)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $userId = $this->getUser()->id;
        $user = $this->database->table('authors')->get($userId);
        if (!$user) {
            $this->error('User nebyl nalezen');
        }
        $this['userForm']->setDefaults($user->toArray());
    }


    public function userFormSucceeded($form, $values)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->error('Pro editování údajů se musíte přihlásit.');
        }
        $userId = $this->getUser()->id;
        if ($userId) {
            $user = $this->database->table('authors')->get($userId);
            $file = $values['photo'];
            if($file->isImage() and $file->isOk()) {
                $file_ext=strtolower(mb_substr($file->getSanitizedName(), strrpos($file->getSanitizedName(), ".")));
                $file_name = uniqid(rand(0,20), TRUE).$file_ext;
                $file->move('images/'. $file_name);
            } else {
                unset($values['photo']);
            }
            $user->update($values);
        } else {
            $user = $this->database->table('authors')->insert($values);

        }

        $this->flashMessage('Údaje byly úspěšně upraveny.', 'alert-success');
        $this->redirect('Homepage:default');
    }

    protected function createComponentUserForm()
    {
        $form = new Form;

        $form->addText('surname', 'Příjmení:')
            ->setRequired();
        $form->addText('name','Jméno:')
            ->setRequired();
        $form->addTextArea('position', 'Popis pozice:')
            ->setRequired();
        $form->addUpload('photo', 'Obrázek:')
            ->addCondition(Form::FILLED)
            ->addRule(Form::IMAGE, 'File must be an image - .jpg, .gif, .png')
            ->addRule(Form::MAX_FILE_SIZE, 'File size must be maximum 2Mb.', 2147483648);
        $form->addCheckbox('work_status', 'Je stále zaměstnancem?');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'userFormSucceeded'];
        return $form;
    }

}