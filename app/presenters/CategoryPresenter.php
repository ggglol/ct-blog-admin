<?php
namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;

class CategoryPresenter extends Nette\Application\UI\Presenter
{
    private $database;
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderShow($userId)
    {
        /* $userId = $this->getUser()->id;
        $user = $this->database->table('authors')->get($userId);
        if (!$user) {
            $this->error('User nebyl nalezen');
        }
        $this->template->user = $user; */
    }

    public function actionEdit($categoryId)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }

        $category = $this->database->table('category')->get($categoryId);
        if (!$category) {
            $this->error('Kategorie nebyla nalezena');
        }
        $this['categoryForm']->setDefaults($category->toArray());
    }
    public function categoryFormSucceeded($form, $values)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->error('Pro editování údajů se musíte přihlásit.');
        }
        $categoryId = $this->getParameter('categoryId');
        if ($categoryId) {
            $category = $this->database->table('category')->get($categoryId);
            $category->update($values);
        } else {
            $category = $this->database->table('category')->insert($values);

        }

        $this->flashMessage('Údaje byly úspěšně upraveny.', 'alert-success');
        $this->redirect('Homepage:showCategories');
    }
    protected function createComponentCategoryForm()
    {
        $form = new Form;
        $form->addText('name', 'Název:')
            ->setRequired();
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'categoryFormSucceeded'];
        return $form;
    }
}