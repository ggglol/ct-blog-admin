<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Ublaboo\DataGrid\DataGrid;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        /* $this->template->posts = $this->database->table('posts')
            ->order('date DESC')
            ->limit(5); */
    }

    public function createComponentSimpleGridArticles($name)
    {
        DataGrid::$icon_prefix = 'fa fa-';

        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->database->table('posts')->where('id = ', $this->getUser()->id));
        $grid->addColumnText('title', 'Titulek');
        $grid->addColumnText('content', 'Text');
        $grid->setItemsPerPageList([10]);
        $grid->addColumnText('name', 'Kategorie', 'category.name');
        $grid->addColumnDateTime('date','Datum');
        $grid->addColumnStatus('state','Status')
            ->addOption(1, 'Koncept')
            ->setClass('btn-danger')
            ->endOption()
            ->addOption(0, 'Publikován')
            ->setClass('btn-success')
            ->endOption()
            ->onChange[] = function($categoryId, $value) {
            $values['state']=$value;
            $this->database->table('posts')->where('id = ', $categoryId)->update($values);
        };
        //$grid->addColumnLink('id', 'Link', 'Post:show', 'title', ['postId' => 'id']);
        $grid->addAction('edit','', 'Post:edit', ['postId' => 'id'])
            ->setIcon('edit')
            ->setTitle('Upravit');
        $grid->addAction('delete', '')
            ->setIcon('trash')
            ->setTitle('Delete')
            ->setClass('btn btn-xs btn-danger ajax');
    }

    public function createComponentSimpleGridArticlesAdmin($name)
    {
        DataGrid::$icon_prefix = 'fa fa-';

        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->database->table('posts'));
        $grid->addColumnText('title', 'Titulek');
        $grid->addColumnText('content', 'Text');
        $grid->setItemsPerPageList([10]);
        $grid->addColumnText('category','Kategorie', 'category.name');
        $grid->addColumnDateTime('date','Datum');
        $grid->addColumnStatus('state','Status')
            ->addOption(1, 'Koncept')
            ->setClass('btn-danger')
            ->endOption()
            ->addOption(0, 'Publikován')
            ->setClass('btn-success')
            ->endOption()
            ->onChange[] = function($categoryId, $value) {
            $values['state']=$value;
            $this->database->table('posts')->where('id = ', $categoryId)->update($values);
        };
        //$grid->addColumnLink('id', 'Link', 'Post:show', 'title', ['postId' => 'id']);
        $grid->addAction('edit','', 'Post:edit', ['postId' => 'id'])
            ->setIcon('edit')
            ->setTitle('Upravit');
        $grid->addAction('delete', '', 'deletearticle!', ['postId' => 'id'])
            ->setIcon('trash')
            ->setTitle('Delete')
            ->setClass('btn btn-xs btn-danger ajax');

    }

    public function handleDeletearticle($postId){
        $this->database->table('related_posts')->where('post_id = ', $postId)->delete();
        $this->database->table('related_posts')->where('related_id = ', $postId)->delete();
        $this->database->table('related_tags')->where('post_id = ', $postId)->delete();
        $this->database->table('posts')->where('id = ', $postId)->delete();
        $this->redirect('Homepage:');
    }

    public function createComponentSimpleGridUsers($name)
    {
        DataGrid::$icon_prefix = 'fa fa-';

        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->database->table('authors'));
        $grid->setItemsPerPageList([10]);
        $grid->addColumnText('name', 'Jméno');
        $grid->addColumnText('surname', 'Příjmení');
        $grid->addColumnText('position','Popis pozice');
        $grid->addColumnStatus('work_status','Je zaměstnancem?')
            ->addOption(1, 'Ne')
            ->setClass('btn-danger')
            ->endOption()
            ->addOption(0, 'Ano')
            ->setClass('btn-success')
            ->endOption()
            ->onChange[] = function($categoryId, $value) {
            $values['work_status']=$value;
            $this->database->table('authors')->where('id = ', $categoryId)->update($values);
        };
        //$grid->addColumnLink('id', 'Link', 'Post:show', 'title', ['postId' => 'id']);
        $grid->addAction('edit','', 'User:edit', ['userId' => 'id'])
            ->setIcon('edit')
            ->setTitle('Upravit');
    }

    public function createComponentSimpleGridCategories($name)
    {
        DataGrid::$icon_prefix = 'fa fa-';

        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->database->table('category'));
        $grid->setItemsPerPageList([10]);
        $grid->addColumnText('name', 'Název');
        $grid->addColumnStatus('isDeleted','Je smazaná?')
            ->addOption(1, 'Ano')
            ->setClass('btn-danger')
            ->endOption()
            ->addOption(0, 'Ne')
            ->setClass('btn-success')
            ->endOption()
            ->onChange[] = function($categoryId, $value) {
            $values['isDeleted']=$value;
            $this->database->table('category')->where('id = ', $categoryId)->update($values);
        };
        $grid->addAction('edit','', 'Category:edit', ['categoryId' => 'id'])
            ->setIcon('edit')
            ->setTitle('Upravit');
    }

    public function createComponentSimpleGridTags($name)
    {
        DataGrid::$icon_prefix = 'fa fa-';

        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->database->table('tags'));
        $grid->setItemsPerPageList([10]);
        $grid->addColumnText('name', 'Název');
        $grid->addColumnStatus('isDeleted','Je smazaná?')
            ->addOption(1, 'Ano')
            ->setClass('btn-danger')
            ->endOption()
            ->addOption(0, 'Ne')
            ->setClass('btn-success')
            ->endOption()
            ->onChange[] = function($categoryId, $value) {
            $values['isDeleted']=$value;
            $this->database->table('tags')->where('id = ', $categoryId)->update($values);
        };
        $grid->addAction('edit','', 'Tags:edit', ['tagId' => 'id'])
            ->setIcon('edit')
            ->setTitle('Upravit');
    }

}
