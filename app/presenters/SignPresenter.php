<?php

namespace App\Presenters;

use Nette;
use App\Forms;
use Nette\Application\UI\Form;


class SignPresenter extends Nette\Application\UI\Presenter
{
    /** @var Forms\SignInFormFactory @inject */
    public $signInFactory;

    /** @var Forms\SignUpFormFactory @inject */
    public $signUpFactory;


    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */

    protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addText('email', 'Email:')
            ->addRule(Form::PATTERN, 'Email musí mít platný formát', '[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}')
            ->setRequired('Prosím vyplňte svůj email.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
    }

    public function signInFormSucceeded($form, $values)
    {
        try {
            $this->getUser()->login($values->email, $values->password);
            $this->redirect('Homepage:');

        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Nesprávné údaje.', 'alert-danger');
        }
    }


    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignUpForm()
    {
        return $this->signUpFactory->create(function () {
            $this->redirect('Homepage:showUsers');
        });
    }


    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Odhlášení bylo úspěšné.', 'alert-success');
        $this->redirect('Homepage:');
    }

}
