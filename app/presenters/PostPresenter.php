<?php
namespace App\Presenters;

use Nette;
use Nette\Forms\Controls;
use App\Model\ArticleManager;
use Nette\Application\UI\Form;
use Nextras\Forms\Controls\DatePicker;


class PostPresenter extends Nette\Application\UI\Presenter
{
    private $articleManager;

    public function __construct(ArticleManager $articleManager)
    {
        $this->articleManager = $articleManager;
    }

    public function renderShow($postId)
    {

    }

    public function actionEdit($postId)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }

        $post = $this->articleManager->getArticles()->get($postId);

        if (!$post) {
            $this->error('Článek nebyl nalezen');
        }

        $this['postForm']->setDefaults($post->toArray());
    }
    public function postFormSucceeded($form, $values)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->error('Pro editování údajů se musíte přihlásit.');
        }

        $postId = $this->getParameter('postId');
        if ($postId) {
            $values['author_id']=$this->getUser()->id;

            if($values['lead_image']->isImage() and $values['lead_image']->isOk()) {
                $file_ext=strtolower(mb_substr($values['lead_image']->getSanitizedName(), strrpos($values['lead_image']->getSanitizedName(), ".")));
                $file_name = uniqid(rand(0,20), TRUE).$file_ext;
                $values['lead_image']->move('images/'. $file_name);
                $this->articleManager->getArticles()->where('id = ', $postId)->update(['lead_image' => $values['lead_image']]);
            }
            if ($values['lead_image']=='') {
                unset($values['lead_image']);
            }

            $tags_arr = explode(',',$values['tags']);
            $this->articleManager->getRelatedTags()->where('post_id = ', $postId)->delete();
            foreach ($tags_arr as $tag) {
                $ex = $this->articleManager->getTags()->where('name = ', $tag)->fetch();
                if (!$ex){
                    $this->articleManager->getTags()->insert([
                        'name' => $tag,
                    ]);
                } else {
                    unset($values['tags']);
                }
                $this->articleManager->getRelatedTags()->insert([
                    'post_id' => $postId,
                    'tag_id' => $this->articleManager->getTags()->select('id')->where('name = ?', $tag),
                ]);
            }

            $post = $this->articleManager->getArticles()->where('id = ', $postId)->update([
                'title' => $values['title'],
                'slug' => $values['slug'],
                'lead' => $values['lead'],
                'content' => $values['content'],
                'category' => $values['category'],
                'state' => $values['state'],
                'date' => $values['date'],
                'author_id' => $values['author_id']
            ]);

            $this->articleManager->getRelatedArticles()->where('post_id = ', $postId)->delete();

            if($values['related1']){
                $related_post = $this->articleManager->getRelatedArticles()->insert([
                    'post_id' => $postId,
                    'related_id' => $values['related1'],
                ]);
            } else {
                unset($values['related1']);
            }

            if($values['related2']){
                $related_post = $this->articleManager->getRelatedArticles()->insert([
                    'post_id' => $postId,
                    'related_id' => $values['related2'],
                ]);
            } else {
                unset($values['related2']);
            }

            if($values['related3']){
                $related_post = $this->articleManager->getRelatedArticles()->insert([
                    'post_id' => $postId,
                    'related_id' => $values['related3'],
                ]);
            } else {
                unset($values['related3']);
            }

        } else {
            if($values['lead_image']->isImage() and $values['lead_image']->isOk()) {
                $file_ext=strtolower(mb_substr($values['lead_image']->getSanitizedName(), strrpos($values['lead_image']->getSanitizedName(), ".")));
                $file_name = uniqid(rand(0,20), TRUE).$file_ext;
                $values['lead_image']->move('images/'. $file_name);
            }

            $values['author_id']=$this->getUser()->id;

            $post = $this->articleManager->getArticles()->insert([
                'title' => $values['title'],
                'slug' => $values['slug'],
                'lead' => $values['lead'],
                'content' => $values['content'],
                'category' => $values['category'],
                'state' => $values['state'],
                'date' => $values['date'],
                'author_id' => $values['author_id']
            ]);

            if($values['related1']){
                $related_post = $this->articleManager->getRelatedArticles()->insert([
                    'post_id' => $post['id'],
                    'related_id' => $values['related1'],
                ]);
            } else {
                unset($values['related1']);
            }

            if($values['related2']){
                $related_post = $this->articleManager->getRelatedArticles()->insert([
                'post_id' => $post['id'],
                'related_id' => $values['related2'],
            ]);
            } else {
                unset($values['related2']);
            }

            if($values['related3']){
                $related_post = $this->articleManager->getRelatedArticles()->insert([
                    'post_id' => $post['id'],
                    'related_id' => $values['related3'],
                ]);
            } else {
                unset($values['related3']);
            }

            $tags_arr = explode(',',$values['tags']);
            foreach ($tags_arr as $tag) {
                $ex = $this->articleManager->getTags()->where('name = ', $tag)->fetch();
                if (!$ex){
                $this->articleManager->getTags()->insert([
                    'name' => $tag,
                ]);
                } else {
                    unset($values['tags']);
                }
                
                $this->articleManager->getRelatedTags()->insert([
                    'post_id' => $post['id'],
                    'tag_id' => $this->articleManager->getTags()->select('id')->where('name = ?', $tag),
                    ]);
                }
            }

        $this->flashMessage('Údaje byly úspěšně upraveny.', 'alert-success');
        $this->redirect('Homepage:');
    }

    protected function createComponentPostForm()
    {
        $datePicker = new DatePicker;
        $datePicker->setRequired();

        $categories = [];
        foreach ($this->articleManager->getCategories() as $item) {
            $categories[$item['id']] = $item['name'];
        }

        if($this->getParameter('postId')) {
            $this->template->image = $this->articleManager->getArticles()
                ->where('id = ', $this->getParameter('postId'))
                ->fetch()->lead_image;
        }


        $form = new Form;
        $form->addText('title', 'Název:')
            ->setRequired();
        $form->addText('slug','Zadejte slug pro článek')
            ->setRequired();
        $form->addTextArea('lead','Perex:')
            ->setRequired();
        $form->addUpload('lead_image', 'Obrázek:')
            ->addCondition(Form::FILLED, TRUE)
            ->addRule(Form::IMAGE, 'Avatar musí být JPEG, PNG nebo GIF.');
        $form->addTextArea('content', 'Obsah:')
            ->setRequired();
        $form->addSelect('category', 'Kategorie:')
            ->setItems($categories, TRUE)
            ->setPrompt('Vyberte kategorii');

        $relArt = [];
        if($this->getParameter('postId')){
            foreach ($this->articleManager->getRelatedArticles()->where('post_id = ', $this->getParameter('postId')) as $art) {
                foreach ($this->articleManager->getArticles()->where('id = ', $art['related_id']) as $title) {
                    $relArt[$art['related_id']] = $title['title'];
                }
            }

            $posRelArt = [];
            foreach ($this->articleManager->getArticles() as $item1) {
                $posRelArt[$item1['id']] = $item1['title'];
            }

            if (!$relArt){
                for ($i=1; $i<4; $i++){
                    $form->addSelect('related'.$i, '')
                        ->setItems($posRelArt)
                        ->setPrompt('Vyberte článek');
                }
            } else {
                $k = 1;
                foreach ($relArt as $art){
                    $form->addSelect('related'.$k, '')
                        ->setItems($posRelArt)
                        ->setDefaultValue(array_search($art, $relArt));
                    $k++;
                }
            }
        } else {
            $posRelArt = [];
            foreach ($this->articleManager->getArticles() as $item1) {
                $posRelArt[$item1['id']] = $item1['title'];
            }

            for ($i=1; $i<4; $i++){
                $form->addSelect('related'.$i, '')
                    ->setItems($posRelArt)
                    ->setPrompt('Vyberte článek');
            }
        }

        $relatedTags = [];
        if($this->getParameter('postId')) {
            foreach ($this->articleManager->getRelatedTags()->where('post_id = ', $this->getParameter('postId')) as $tag) {
                foreach ($this->articleManager->getTags()->where('id = ', $tag['tag_id']) as $name) {
                    $relatedTags[$tag['tag_id']] = $name['name'];
                }
            }
            $arr = implode(',', $relatedTags);
        $form->addText('tags', 'Štítky:')
            ->setDefaultValue($arr);
        } else {
            $form->addText('tags', 'Štítky:');
        }

        $form->addCheckbox('state', 'Uložit jako koncept');
        $form->addComponent($datePicker, 'date');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'postFormSucceeded'];
        return $form;
    }
}