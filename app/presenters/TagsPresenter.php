<?php
namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;

class TagsPresenter extends Nette\Application\UI\Presenter
{
    private $database;
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderShow($userId)
    {
        /* $userId = $this->getUser()->id;
        $user = $this->database->table('authors')->get($userId);
        if (!$user) {
            $this->error('User nebyl nalezen');
        }
        $this->template->user = $user; */
    }

    public function actionEdit($tagId)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }

        $tag = $this->database->table('tags')->get($tagId);
        if (!$tag) {
            $this->error('Štítek nebyl nalezen');
        }
        $this['tagForm']->setDefaults($tag->toArray());
    }
    public function tagFormSucceeded($form, $values)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->error('Pro editování údajů se musíte přihlásit.');
        }
        $tagId = $this->getParameter('tagId');
        if ($tagId) {
            $tag = $this->database->table('tags')->get($tagId);
            $tag->update($values);
        } else {
            $tag = $this->database->table('tags')->insert($values);

        }

        $this->flashMessage('Údaje byly úspěšně upraveny.', 'alert-success');
        $this->redirect('Homepage:showTags');
    }
    protected function createComponentTagForm()
    {
        $form = new Form;
        $form->addText('name', 'Název:')
            ->setRequired();
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'tagFormSucceeded'];
        return $form;
    }
}