<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model;


class SignUpFormFactory
{
	use Nette\SmartObject;

	const PASSWORD_MIN_LENGTH = 7;
	const EMAIL_EXPR = '[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}';

	/** @var FormFactory */
	private $factory;

	/** @var Model\UserManager */
	private $userManager;


	public function __construct(FormFactory $factory, Model\UserManager $userManager)
	{
		$this->factory = $factory;
		$this->userManager = $userManager;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();

		$role = ['Admin', 'Editor'];

		$form->addText('name','Jméno')
            ->setRequired();

		$form->addText('surname','Příjmení')
            ->setRequired();

		$form->addText('email', 'Email:')
            ->addRule($form::PATTERN, 'Email musí mít platný formát', self::EMAIL_EXPR)
			->setRequired('Zadejte mail.');

		$form->addPassword('password', 'Heslo:')
			->setOption('description', sprintf('alespoň %d znaků', self::PASSWORD_MIN_LENGTH))
			->setRequired('Zadejte heslo.')
			->addRule($form::MIN_LENGTH, NULL, self::PASSWORD_MIN_LENGTH);

		$form->addSelect('role', 'Role:')
            ->setItems($role, FALSE);

		$form->addTextArea('position','Popis pozice')
            ->setRequired();

		$form->addSubmit('send', 'Sign up');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->userManager->add($values->email, $values->password, $values->role, $values->name, $values->surname, $values->position, $values->photo);
			} catch (Model\DuplicateNameException $e) {
				$form['email']->addError('Email v databázi již existuje.');
				return;
			}
			$onSuccess();
		};

		return $form;
	}

}
