<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'authors',
		COLUMN_ID = 'id',
		COLUMN_NAME = 'email',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_ROLE = 'role',
        COLUMN_1 = 'name',
        COLUMN_2 = 'surname',
	    COLUMN_3 = 'position',
	    COLUMN_4 = 'photo';



	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;

		$row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $email)->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('Email není platný.', self::IDENTITY_NOT_FOUND);

		} elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('Heslo není platné.', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
			$row->update([
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
			]);
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}


	public function add($email, $password, $role, $name, $surname, $position, $photo)
	{
	    try {
			$this->database->table(self::TABLE_NAME)->insert([
				self::COLUMN_NAME => $email,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
                self::COLUMN_ROLE => $role,
                self::COLUMN_1 => $name,
                self::COLUMN_2 => $surname,
                self::COLUMN_3 => $position,
                self::COLUMN_4 => $photo,
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

}



class DuplicateNameException extends \Exception
{}
