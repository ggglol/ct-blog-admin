<?php

namespace App\Model;

use Nette;

class ArticleManager
{
use Nette\SmartObject;

/**
* @var Nette\Database\Context
*/
private $database;

public function __construct(Nette\Database\Context $database)
{
    $this->database = $database;
}

public function getArticles(){
    return $this->database->table('posts');
}

public function getCategories(){
    return $this->database->table('category')
        ->fetchAssoc('name');
}

public function getRelatedArticles(){
    return $this->database->table('related_posts');
}

//public function getAll(){
//    return $this->database;
//}

public function getTags(){
    return $this->database->table('tags');
}

public function getRelatedTags(){
    return $this->database->table('related_tags');

}

}